﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/MiEstilo.css" rel="stylesheet" />
    <br /><br /><hr />
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item">
                <asp:Image src="img/s1.png" ID="Image2" Width="1500px" CssClass="img img-responsive" runat="server" />
                <%--<div class="carousel-caption">
                    <h3>
                        <asp:Label ID="Label3" runat="server" Text="Comida para perro"></asp:Label>
                    </h3>
                    <p>A un precio de oferta de:
                        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="Label5" runat="server" Text="Label" Style="text-decoration: line-through"></asp:Label>
                    </p>
                    <a href="alimentosp.aspx">Mas Información</a>
                </div>--%>
            </div>
            <div class="item active">
                <asp:Image src="img/s2.png" ID="ImgProd1" Width="1500px" CssClass="img img-responsive" runat="server" />
                <div class="carousel-caption">
                    <h3>
                        <asp:Label ID="lblProdNombre1" runat="server" Text="Comida para perro"></asp:Label>
                    </h3>
                    <p>Hasta con un 40% de descuento
                        <%--<asp:Label ID="lblProdPrecioOferta1" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="lblPrecioNorm1" runat="server" Text="Label" Style="text-decoration: line-through"></asp:Label>--%>
                    </p>
                    <a href="ofertas.aspx">Mas Información</a>
                </div>
            </div>

            <div class="item">
                <asp:Image src="img/s3.png" ID="Image1" Width="1500px" CssClass="img img-responsive" runat="server" />
                <%--<div class="carousel-caption">
                    <h3>
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                    </h3>
                    <p>A un precio de oferta de:
                        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="Label6" runat="server" Text="Label" Style="text-decoration: line-through"></asp:Label>
                    </p>
                    <a href="casas.aspx">Mas Información</a>
                </div>--%>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
        </a>
    </div><!-- hasta aqui slider -->
    <br /><br />
    <div>
       <asp:Image src="img/op.png" ID="Image3" Width="1500px" CssClass="img img-responsive" runat="server" />
    </div>
    <br /><br /><hr />
    <div class="row">
        <div class="col-md-4">
            <h2>La influencia de las mascotas en la vida humana</h2>
            <p>
                La influencia positiva de las mascotas en la salud y bienestar de los seres humanos es bien reconocida y 
                comprende los aspectos psicológico, fisiológico, terapéutico y sicosocial. La función como facilitadores en la
                terapia asistida motivacional y física de numerosas enfermedades, ha permitido que los efectos benéficos de
                la tenencia de animales sean empleados en el ámbito terapéutico. 
            </p>
            <p>
                <a class="btn btn-default" href="http://www.scielo.org.co/pdf/rccp/v20n3/v20n3a16.pdf">Leer mas &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Extinción <br />Breve repaso por la historia</h2>
            <p>
                Hace muchísimos años, los efectos de la humanidad sobre su entorno eran pocos y localizados. La capacidad de animales 
                y plantas para regenerarse, era más fácil frente a las actividades de caza y recolección. Pero el crecimiento exponencial 
                de la población humana, ha modificado esta situación hasta el punto de que los ecosistemas ya no pueden recuperarse. 
                ¿Por qué debemos preocuparnos por la biodiversidad? ¿Es tan importante?
            </p>
            <p>
                <a class="btn btn-default" href="http://deanimalia.com/extincion.html">Leer mas &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Cómo eliminar las pulgas de mi mascota</h2>
            <p>
                Las pulgas pueden ser un problema incluso en el hogar más impecable o en el animal doméstico más limpio.<br />
                Tratar tanto su mascota y su casa, ya que las pulgas pueden sobrevivir sin un huésped durante muchos meses. 
                Visite a su veterinario para el consejo sobre los mejores productos...
            </p><br /><br /><br />
            <p>
                <a class="btn btn-default" href="http://www.mascotadomestica.com/articulos-sobre-mascotas/como-eliminar-las-pulgas-de-mi-mascota.html">Leer mas &raquo;</a>
            </p>
        </div>
    </div>
</asp:Content>
