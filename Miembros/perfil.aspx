﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="perfil.aspx.vb" Inherits="Miembros_perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br /><hr />
    <div class="form-horizontal">
        <h4>Verifica tus datos</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblnom" CssClass="col-md-2 control-label">Nombre de usuario</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lblnom" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblmail" CssClass="col-md-2 control-label">E-mail</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lblmail" TextMode="Email" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lbltel" CssClass="col-md-2 control-label">Teléfono</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lbltel" TextMode="Phone" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lbldir" CssClass="col-md-2 control-label">Dirección</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lbldir" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblcd" CssClass="col-md-2 control-label">Ciudad</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lblcd" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblcp" CssClass="col-md-2 control-label">C.P</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lblcp" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" Text="Editar mis datos" CssClass="btn btn-default" ID="btnedit" />
                <asp:Button runat="server" Text="Seguir Comprando" CssClass="btn btn-default" ID="btncont"/>
                <asp:Button runat="server" Text="Finalizar Compra" CssClass="btn btn-default" ID="btnf"/>
            </div>
        </div>
    </div>

</asp:Content>

