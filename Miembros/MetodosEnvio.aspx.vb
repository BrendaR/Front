﻿
Partial Class Carrito_MetodosEnvio
    Inherits System.Web.UI.Page

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("/Carrito/Carrito.aspx")
    End Sub

    Private Sub Carrito_MetodosEnvio_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim conectado As String
        conectado = Context.User.Identity.GetUserName()

        Session("conectado") = conectado

        Dim Envio As String
        Dim Costo As Double
        Dim Pago As String

        If rb1.Checked Then
            Envio = rb1.Text
            Costo = "0"
        End If

        If rb2.Checked Then
            Envio = rb2.Text
            Costo = "150"
        End If

        If rb3.Checked Then
            Envio = rb3.Text
            Costo = "200"
        End If

        If rb4.Checked Then
            Envio = rb4.Text
            Costo = "180"
        End If

        'establecer los valores en los controles correspondientes
        Session("Pago") = ddlPago.SelectedItem.Text
        Session("Envio") = Envio
        Session("Costo") = Costo
    End Sub

    'Private Sub btnContinuar_Click(sender As Object, e As EventArgs) Handles btnContinuar.Click
    '    Dim conectado As String
    '    conectado = Context.User.Identity.GetUserName()

    '    Session("conectado") = conectado

    '    Dim Envio As String
    '    Dim Costo As Double
    '    Dim Pago As String

    '    If rb1.Checked Then
    '        Envio = rb1.Text
    '        Costo = "0"
    '    End If

    '    If rb2.Checked Then
    '        Envio = rb2.Text
    '        Costo = "150"
    '    End If

    '    If rb3.Checked Then
    '        Envio = rb3.Text
    '        Costo = "200"
    '    End If

    '    If rb4.Checked Then
    '        Envio = rb4.Text
    '        Costo = "180"
    '    End If

    '    'establecer los valores en los controles correspondientes
    '    Session("Pago") = ddlPago.SelectedItem.Text
    '    Session("Envio") = Envio
    '    Session("Costo") = Costo
    'End Sub

    Private Sub Carrito_MetodosEnvio_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

        lblMensajeriaEnvio.Text = Session("Envio")
        lblMetodoPago.Text = Session("Pago")

        lblTotal.Text = FormatCurrency(Session("Total"))
        lblEnvio.Text = FormatCurrency(Session("Costo"))

        lblTotalPagar.Text = FormatCurrency(Session("Total") + Session("Costo"))
        lblTotalPagarT.Text = lblTotalPagar.Text

    End Sub

    Private Sub btnPagar_Click(sender As Object, e As EventArgs) Handles btnPagar.Click
        Dim n As Integer
        Dim usuario As String

        usuario = Context.User.Identity.GetUserName()

        Dim Carrito As New dsProductos.CARRITODataTable
        Dim TA As New dsProductosTableAdapters.CARRITOTableAdapter
        Carrito = TA.GetDataByNombre(usuario)

        Dim TADet As New dsProductosTableAdapters.DET_VENTATableAdapter

        Dim Venta As New dsProductos.VENTASDataTable
        Dim TAVenta As New dsProductosTableAdapters.VENTASTableAdapter

        Dim id As Integer
        Dim fecha As Date
        fecha = Now



        n = TA.Contar(usuario) 'contar productos que tiene el carrito segun usuario

        TAVenta.InsertarVenta(Carrito(0).NOMBRE_USUARIO, Session("Total") + Session("Costo"), fecha) 'insert en ventas
        Venta = TAVenta.GetDataByUsuario(usuario, fecha) 'consultar datos de la venta segun nombre de usuario y fecha

        For i = 1 To n
            id = Venta(0).ID_VENTA 'sacar el id de la venta
            TADet.InsertarDetalle(Carrito(i - 1).CANTIDAD, Carrito(i - 1).PRECIO, Carrito(i - 1).SUBTOTAL, fecha, Carrito(i - 1).ID_PRODUCTO, Session("Envio"), Session("Pago"), id)
            TA.Eliminar(Carrito(i - 1).ID)
        Next i
        Response.Redirect("/Miembros/Ticket.aspx")
    End Sub
End Class
