﻿
Partial Class Perrito_bajas
    Inherits System.Web.UI.Page

    Private Sub FormView1_ItemDeleting(sender As Object, e As FormViewDeleteEventArgs) Handles FormView1.ItemDeleting
        Dim respuesta As MsgBoxResult

        respuesta = MsgBox("Esta seguro que desea eliminar el producto", vbQuestion + vbYesNo, "Borrar producto")

        If respuesta = vbNo Then
            e.Cancel = True
        End If

    End Sub
End Class
