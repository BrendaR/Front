﻿<%@ Page Title="" Language="VB" MasterPageFile="SiteA.master" AutoEventWireup="false" CodeFile="consultas.aspx.vb" Inherits="Consultas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br />
    <br />
    <h2>Detalles de Productos</h2>
    <hr />
    <asp:DataList ID="DataList1" runat="server" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal" DataSourceID="SqlDataSourceDataList">
        <ItemTemplate>
            <div class="col-md-4">
                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("IMAGEN") %>' Width="150px" Height="150px" />
                <br />
                NOMBRE:
            <asp:Label Text='<%# Eval("NOMBRE") %>' runat="server" ID="NOMBRELabel" />
                <br />
                DESCRIPCION:
            <asp:Label Text='<%# Eval("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
                PRECIO_NORMAL:
            <asp:Label Text='<%# Eval("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALLabel" /><br />
                OFERTA:
            <asp:Label Text='<%# Eval("OFERTA") %>' runat="server" ID="OFERTALabel" /><br />
                PRECIO_OFERTA:
            <asp:Label Text='<%# Eval("PRECIO_OFERTA") %>' runat="server" ID="PRECIO_OFERTALabel" /><br />
                <br />
            </div>
            <br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceDataList" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [OFERTA], [PRECIO_OFERTA], [IMAGEN] FROM [PRODUCTOS] ORDER BY [NOMBRE]"></asp:SqlDataSource>
</asp:Content>

