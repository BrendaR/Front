﻿Partial Class Carrito_AgregarCarrito
    Inherits System.Web.UI.Page

    Private Sub Carrito_AgregarCarrito_Load(sender As Object, e As EventArgs) Handles Me.Load

        'recuperar Id del producto

        Dim IDProd As Integer
        IDProd = Request("IDProd")

        'recuperar los datos del productos

        Dim Productos As New dsProductos.PRODUCTOSDataTable
        Dim TA As New dsProductosTableAdapters.PRODUCTOSTableAdapter
        Productos = TA.GetDataByIDProducto(IDProd)

        imgProducto.ImageUrl = Productos(0).IMAGEN
        txtNombreProducto.Text = Productos(0).NOMBRE
        lblPrecio.Text = Format(Productos(0).PRECIO_NORMAL, "Currency")

        lblSubtotal.Text = Format((lblPrecio.Text * ddlCantidad.SelectedValue), "Currency")

    End Sub

    Private Sub ddlCantidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCantidad.SelectedIndexChanged
        lblSubtotal.Text = Format((lblPrecio.Text * ddlCantidad.SelectedValue), "Currency")
    End Sub

    Private Sub btnAgregarCarrito_Click(sender As Object, e As EventArgs) Handles btnAgregarCarrito.Click

        Dim Cantidad As Integer
        Dim Precio As Decimal
        Dim SubTotal As Decimal
        Dim Fecha As Date
        Dim ID_Prod As Integer
        Dim Nombre_Usuario As String

        Nombre_Usuario = User.Identity.Name
        ID_Prod = Request("IDProd")
        Fecha = Now
        Cantidad = ddlCantidad.SelectedValue

        'recuperar Id del producto


        'recuperar los datos del productos

        Dim Productos As New dsProductos.PRODUCTOSDataTable
        Dim TA As New dsProductosTableAdapters.PRODUCTOSTableAdapter
        Productos = TA.GetDataByIDProducto(ID_Prod)

        'campos necesarios para agregar al carrito

        Precio = Productos(0).PRECIO_NORMAL
        SubTotal = Cantidad * Precio 'Format((lblPrecio.Text * ddlCantidad.SelectedValue), "Currency")

        'agregar los datos a la tabla carrito
        Dim Carrito As New dsProductos.CARRITODataTable
        Dim TACarrito As New dsProductosTableAdapters.CARRITOTableAdapter

        TACarrito.Insertar(Cantidad, Precio, SubTotal, Fecha, ID_Prod, Nombre_Usuario)

        'Mostrar mensaje de que ya se inserto y redirigir usuario a seguir comprando
        Response.Redirect("~/Miembros/Carrito.aspx")

    End Sub
End Class