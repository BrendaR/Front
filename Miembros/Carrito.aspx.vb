﻿
Partial Class Carrito_Carrito
    Inherits System.Web.UI.Page

    Private Sub Carrito_Carrito_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("SCaja") = User.Identity.Name

        'recuperar el total del usuario

        Dim Usuario As String

        Usuario = User.Identity.Name

        Dim TA As New dsProductosTableAdapters.CARRITOTableAdapter
        Session("Total") = FormatCurrency(TA.TotalUsuario(Usuario))
        lblTotal.Text = Session("Total")

    End Sub

    Private Sub DataList1_PreRender(sender As Object, e As EventArgs) Handles DataList1.PreRender
        Dim IDProd As Label
        Dim NombreProd As Label

        Try
            IDProd = DataList1.FindControl("ID")
            NombreProd = DataList1.FindControl("NOMBRE")
            lblNR.Text = IDProd.Text & "Con nombre" & NombreProd.Text
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnPagar_Click(sender As Object, e As EventArgs) Handles btnPagar.Click
        'depende de la logica de la aplicacion la forma de pago

        'mostrar la lista de opciones de envio
        Response.Redirect("MetodosEnvio.aspx")

        'mostrarle al usuario sus datos generales
    End Sub
End Class

