﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="carro.aspx.vb" Inherits="carro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <h2>Datos de Pago</h2>
    <hr />
    <br />
        <div class="form-group">
            <label for="select" class="col-lg-2 control-label">Forma de Pago</label>
            <div class="col-lg-10" id="cl">
                <select class="form-control">
                    <option>Tarjeta</option>
                    <option>Oxxo</option>
                </select>
            </div>
        </div><br /><br />
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" Text="Comprar" CssClass="btn btn-default" ID="btncomp"/>
            </div>
        </div>

</asp:Content>

