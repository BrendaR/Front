﻿
Partial Class Carrito_ModificarCarrito
    Inherits System.Web.UI.Page

    Private Sub Carrito_ModificarCarrito_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'recuperar Id del producto

            Dim IDProd As Integer
            IDProd = Request("IDC")

            'recuperar los datos del carrito

            Dim Carrito As New dsProductos.CARRITODataTable
            Dim TA As New dsProductosTableAdapters.CARRITOTableAdapter

            Carrito = TA.GetDataByIDCarrito(IDProd)

            'actualizar el carrito con los nuevos valores

            Dim Cantidad As Integer
            Dim Precio As Decimal
            Dim SubTotal As Decimal
            Dim Fecha As Date

            ddlCantidad.SelectedValue = Carrito(0).CANTIDAD

            lblPrecio.Text = Carrito(0).PRECIO

            Dim Productos As New dsProductos.PRODUCTOSDataTable
            Dim TAProd As New dsProductosTableAdapters.PRODUCTOSTableAdapter

            Productos = TAProd.GetDataByIDProducto(Carrito(0).ID_PRODUCTO)

            imgProducto.ImageUrl = Productos(0).IMAGEN
            txtNombreProducto.Text = Productos(0).NOMBRE

            'imgProducto.ImageUrl = Carrito(0).IMAGEN
            'txtNombreProducto.Text = Carrito(0).NOMBRE
            'lblPrecio.Text = Format(Carrito(0).PRECIO_NORMAL, "Currency")

            lblSubtotal.Text = Format((lblPrecio.Text * ddlCantidad.SelectedValue), "Currency")
        End If


    End Sub

    Private Sub ddlCantidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCantidad.SelectedIndexChanged

        lblSubtotal.Text = Format((lblPrecio.Text * ddlCantidad.SelectedValue), "Currency")

    End Sub

    Private Sub btnModificarCarrito_Click(sender As Object, e As EventArgs) Handles btnModificarCarrito.Click
        Dim Cantidad As Integer
        Dim SubTotal As Decimal
        Dim Fecha As Date

        Dim ID_Carrito As Integer


        Fecha = Now
        Cantidad = ddlCantidad.SelectedValue
        SubTotal = lblPrecio.Text * Cantidad
        ID_Carrito = Request("IDC")


        'agregar los datos a la tabla carrito
        Dim Carrito As New dsProductos.CARRITODataTable
        Dim TACarrito As New dsProductosTableAdapters.CARRITOTableAdapter

        TACarrito.ModificaCantidad(Cantidad, SubTotal, Fecha, ID_Carrito)

        'Mostrar mensaje de que ya se inserto y redirigir usuario a seguir comprando
        Response.Redirect("/Miembros/Carrito.aspx")
    End Sub
End Class
