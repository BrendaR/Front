﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="accesoriosg.aspx.vb" Inherits="accesoriosg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <br /><br />
    <br />

    <asp:DataList ID="DataList1" runat="server" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal" DataSourceID="SqlDataSourceCasasP">
        <ItemTemplate>
            <div class="col-md-4">
                <asp:Image ID="Image1" runat="server" Height="150px" ImageUrl='<%# Eval("IMAGEN") %>' Width="150px" />
                <br />
                NOMBRE: <asp:Label Text='<%# Eval("NOMBRE") %>' runat="server" ID="NOMBRELabel" /><br />
                DESCRIPCION: <asp:Label Text='<%# Eval("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
                PRECIO: <asp:Label Text='<%# Eval("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALLabel" />
                <br />
                <a href='<%# "detallecasasp.aspx?IDProd=" & Eval("ID_PRODUCTO") %>' class="btn btn-success btn-small">Mas información</a>
                <asp:Button ID="btnAgregarCarrito" runat="server" CssClass="btn btn-primary" PostBackUrl='<%# "~/Miembros/AgregarCarrito.aspx?IDProd=" & Eval("ID_PRODUCTO") %>' Text="Agregar al carro" />
            </div><br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceCasasP" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [ID_PRODUCTO], [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [IMAGEN] FROM [PRODUCTOS] WHERE ([ID_CATEGORIA] = @ID_CATEGORIA)">
        <SelectParameters>
            <asp:Parameter DefaultValue="6" Name="ID_CATEGORIA" Type="Int32"></asp:Parameter>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

