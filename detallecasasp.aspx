﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="detallecasasp.aspx.vb" Inherits="detallecasasp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="Content/MisEstilos.css" rel="stylesheet" />
    <br />
    <br />
    <hr />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID_PRODUCTO" DataSourceID="SqlDataSourceProducto" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal">
        <EditItemTemplate>
            IMAGEN:
            <asp:TextBox Text='<%# Bind("IMAGEN") %>' runat="server" ID="IMAGENTextBox" /><br />
            NOMBRE:
            <asp:TextBox Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRETextBox" /><br />
            DESCRIPCION:
            <asp:TextBox Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONTextBox" /><br />
            PRECIO_NORMAL:
            <asp:TextBox Text='<%# Bind("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALTextBox" /><br />
            ID_PRODUCTO:
            <asp:Label Text='<%# Eval("ID_PRODUCTO") %>' runat="server" ID="ID_PRODUCTOLabel1" /><br />
            <asp:LinkButton runat="server" Text="Actualizar" CommandName="Update" ID="UpdateButton" CausesValidation="True" /><asp:LinkButton runat="server" Text="Cancelar" CommandName="Cancel" ID="UpdateCancelButton" CausesValidation="False" />
        </EditItemTemplate>
        <InsertItemTemplate>
            IMAGEN:
            <asp:TextBox Text='<%# Bind("IMAGEN") %>' runat="server" ID="IMAGENTextBox" /><br />
            NOMBRE:
            <asp:TextBox Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRETextBox" /><br />
            DESCRIPCION:
            <asp:TextBox Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONTextBox" /><br />
            PRECIO_NORMAL:
            <asp:TextBox Text='<%# Bind("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALTextBox" /><br />

            <asp:LinkButton runat="server" Text="Insertar" CommandName="Insert" ID="InsertButton" CausesValidation="True" />&nbsp;<asp:LinkButton runat="server" Text="Cancelar" CommandName="Cancel" ID="InsertCancelButton" CausesValidation="False" />
        </InsertItemTemplate>
        <ItemTemplate>
            <%--<div class="col-md-4">--%>
                <div class="col-md-12">
                    <div class="imgProd">
                        <!-- Button trigger modal -->
                        <a data-toggle="modal" data-target="#exampleModal">
                            <asp:Image ID="imgProd" runat="server" ImageUrl='<%# Eval("IMAGEN") %>' Height="150px" Width="150px" CssClass="img-responsive imgProd" />
                        </a>
                    </div>
                    <br />
                    <h3>
                        <asp:Label Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRELabel" /></h3>
                    <br />
                    <asp:Label Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
                    <asp:Label Text='<%# Bind("PRECIO_NORMAL", "{0:C}") %>' runat="server" ID="PRECIO_NORMALLabel" /><br />
                    <br />
                    <asp:Button ID="btnAgregarCarrito" runat="server" CssClass="btn btn-primary" PostBackUrl='<%# "~/Miembros/AgregarCarrito.aspx?IDProd=" & Eval("ID_PRODUCTO") %>' Text="Agregar al carro" />

                </div>
                <%--</div>--%>
        </ItemTemplate>
    </asp:FormView>

    <asp:SqlDataSource runat="server" ID="SqlDataSourceProducto" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [IMAGEN], [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [ID_PRODUCTO] FROM [PRODUCTOS] WHERE ([ID_PRODUCTO] = @ID_PRODUCTO)">
        <SelectParameters>
            <asp:QueryStringParameter QueryStringField="IDProd" Name="ID_PRODUCTO" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">
                        <%--<asp:Label ID="lblNombre" runat="server" Text=""></asp:Label>--%></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%--<asp:Image ID="imgProdGrande" CssClass="img-responsive" runat="server" Height="300" Width="300" />--%>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>   

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">
                        <asp:Label ID="lblNombre" runat="server" Text=""></asp:Label></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <asp:Image ID="imgProdGrande" CssClass="img-responsive imgProdModal" runat="server" Height="300" Width="300" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <asp:Button ID="btnAgregarCarrito" runat="server" Text="Agregar Carrito" CssClass="btn btn-primary"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
