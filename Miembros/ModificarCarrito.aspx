﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ModificarCarrito.aspx.vb" Inherits="Carrito_ModificarCarrito" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

     <br /><br />
    <hr />
    <h3>Modificar cantidad de productos: </h3>
    <h2><%: Context.User.Identity.GetUserName()  %></h2>
    <h4>Agregar producto al carrito</h4>
    <div class="form-horizontal">
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Image ID="imgProducto" runat="server" />
            </div>
        </div>
        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtNombreProducto" CssClass="col-md-2 control-label" Font-Bold="True">Producto:</asp:Label>
            <div class="col-md-10">
                <asp:Label runat="server" ID="txtNombreProducto" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblPrecio" CssClass="col-md-2 control-label">Precio Unitario:</asp:Label>
            <div class="col-md-10">
                <asp:Label runat="server" ID="lblPrecio" CssClass="form-control" Text="1238.57"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlCantidad" CssClass="col-md-2 control-label">Cantidad:</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList ID="ddlCantidad" runat="server" AutoPostBack="True">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblSubtotal" CssClass="col-md-2 control-label"> Subtotal:</asp:Label>
            <div class="col-md-10">
                <asp:Label runat="server" ID="lblSubtotal" CssClass="form-control" Text="1238.57" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button ID="btnModificarCarrito" runat="server" Text="Modificar Carrito" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
    <hr />


</asp:Content>