﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="contacto.aspx.vb" Inherits="contacto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br />
    <div class="row">
        <div class="col-md-6">
            <h2 style="text-align:center">Quienes somos?</h2>
            <hr />
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <asp:Image src="img/logon.png" CssClass="img img-responsive" Width="150px" ID="Image1" runat="server" />
            </div><br />
            <p style="text-align:center; font-size:large">Somos una empresa dedicada a mantener el bienestar y la comodidad de tu mascota
                ofreciéndote productos de alta calidad al mejor precio, además recibiéndolos en la 
                comodidad de tu hogar de forma rápida y segura, además te brindamos distintas opciones
                de pago para que elijas las que mejor se adapte a ti...<br />
                Tu mejor opción para ti y ellos!!!
            </p>
        </div>
        <div class="col-md-6">
            <h2 style="text-align:center">Contáctanos</h2>
            <hr />
            <p style="text-align:center; font-size:large; font-weight:bold"><img src="img/whats.png" alt="Responsive image" width="30"/>  Síguenos en: 351-111-00-00</p>
            <p style="text-align:center; font-size:large; font-weight:bold"><img src="img/fb.png" alt="Responsive image" width="25"/>   Dudas? Te ayudamos en <a href="facebook.com">Animarket Oficial</a></p>
        </div>
    </div>
</asp:Content>

