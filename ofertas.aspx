﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ofertas.aspx.vb" Inherits="ofertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br /><hr />
    <h1><asp:Label ID="lblOfertas" runat="server" Text="Las mejores ofertas solo aquí!!"></asp:Label></h1>
    <hr />
    <asp:DataList ID="DataList1" runat="server" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal" DataSourceID="SqlDataSourceOfertas">
        <ItemTemplate>
            <div class="col-md-4">
                <asp:Image ID="Image1" runat="server" Height="150px" ImageUrl='<%# Eval("IMAGEN") %>' Width="150px" />
                <br />
                NOMBRE:
            <asp:Label Text='<%# Eval("NOMBRE") %>' runat="server" ID="NOMBRELabel" /><br />
                DESCRIPCION:
            <asp:Label Text='<%# Eval("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
                PRECIO NORMAL:
            <asp:Label Text='<%# Eval("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALLabel" /><br />
                PRECIO OFERTA:
            <asp:Label Text='<%# Eval("PRECIO_OFERTA") %>' runat="server" ID="PRECIO_OFERTALabel" /><br />
                <a href='<%# "detallecasasp.aspx?IDProd=" & Eval("ID_PRODUCTO") %>' class="btn btn-success btn-small">Mas información</a>
                <asp:Button ID="btnAgregarCarrito" runat="server" CssClass="btn btn-primary" PostBackUrl='<%# "~/Miembros/AgregarCarrito.aspx?IDProd=" & Eval("ID_PRODUCTO") %>' Text="Agregar al carro" />
                <br />
            </div>
            <br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceOfertas" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [ID_PRODUCTO], [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [PRECIO_OFERTA], [IMAGEN] FROM [PRODUCTOS] WHERE ([OFERTA] = @OFERTA)">
        <SelectParameters>
            <asp:Parameter DefaultValue="true" Name="OFERTA" Type="Boolean"></asp:Parameter>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

