﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="buscar.aspx.vb" Inherits="buscar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br /><hr />

    <asp:DataList ID="DataList1" runat="server" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal" DataSourceID="SqlDataSourcebus">
        <ItemTemplate>
            <div class="col-md-4">
                <asp:Image ID="Image1" runat="server" Height="150px" ImageUrl='<%# Eval("IMAGEN") %>' Width="150px" />
                <br />
                NOMBRE: <asp:Label Text='<%# Eval("NOMBRE") %>' runat="server" ID="NOMBRELabel" /><br />
                DESCRIPCIÓN: <asp:Label Text='<%# Eval("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
                PRECIO: <asp:Label Text='<%# Eval("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALLabel" />
                <br />
                <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Font-Bold="True" Text="Agregar al carro" />
            </div><br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource runat="server" ID="SqlDataSourcebus" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [IMAGEN] FROM [PRODUCTOS] WHERE ([NOMBRE] LIKE '%' + @NOMBRE + '%')">
        <SelectParameters>
            <asp:SessionParameter SessionField="pBuscar" Name="NOMBRE" Type="String"></asp:SessionParameter>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

