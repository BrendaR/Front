﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Carrito.aspx.vb" Inherits="Carrito_Carrito" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br />
    <hr />
    <div class="row">
        <div class="col-md-8">
            <h2>Carrito de compras de: </h2>
            <h3><%: Context.User.Identity.GetUserName()  %></h3>
        </div>
        <div class="col-md-4">
            <h3>Usted lleva un total de: </h3>
            <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label><br />

            <asp:Button ID="btnPagar" CssClass="btn btn-success" runat="server" Text="Pagar" />
        </div>
    </div>

    <br /><br />
    <hr />
    <%--ver productos en el carrito en un datalist solo para eliminar--%>
    <asp:DataList ID="DataList1" runat="server"  DataKeyField="ID" DataSourceID="SqlDataSourceCarrito" CssClass="row" RepeatLayout="Flow" RepeatDirection="Horizontal">
        <ItemTemplate>
            <div class="col-md-4 col-sm-4">
                
            <asp:Label Text='<%# Eval("ID") %>' runat="server" ID="ID" Visible="false" /><br />
                <div class="text-center">
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("IMAGEN") %>'  Width="150px" Height="150px"/><br />
                    <asp:Label Text='<%# Eval("NOMBRE") %>' runat="server" ID="NOMBRE" Font-Bold="true" /><br />

                    AGREGADO EL:
                    <asp:Label Text='<%# Eval("FECHA") %>' runat="server" ID="Label2" Font-Bold="true" /><br />
                    
                    CANTIDAD:
                    <asp:Label Text='<%# Eval("CANTIDAD") %>' runat="server" ID="Label1" Font-Bold="true" /><br />

      
                            PRECIO UNITARIO:
                    <asp:Label Text='<%# Eval("PRECIO", "{0:C}") %>' runat="server" ID="PRECIOLabel" /><br />
                            SUBTOTAL:
                    <asp:Label Text='<%# Eval("SUBTOTAL", "{0:C}") %>' runat="server" ID="SUBTOTALLabel" /><br />

                    <%--DOS PARAMETROS<asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" PostBackUrl='<%# "~/Carrito/ConfirmacionEliminar.aspx?IDProd=" & Eval("IDC") & "&IDC=" & Eval("ID_PRODUCTO") %>'/>--%>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" PostBackUrl='<%# "~/Miembros/ConfirmacionEliminar.aspx?IDC=" & Eval("ID")  %>'/>
                     <asp:Button ID="btnModificar" runat="server" Text="Actualizar" CssClass="btn btn-primary" PostBackUrl='<%# "~/Miembros/ModificarCarrito.aspx?IDC=" & Eval("ID")  %>'/>
                </div>
                
            </div>
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceCarrito" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT CARRITO.ID, CARRITO.CANTIDAD, CARRITO.PRECIO, CARRITO.SUBTOTAL, CARRITO.FECHA, CARRITO.ID_PRODUCTO, CARRITO.NOMBRE_USUARIO, PRODUCTOS.NOMBRE, PRODUCTOS.IMAGEN FROM CARRITO INNER JOIN PRODUCTOS ON CARRITO.ID_PRODUCTO = PRODUCTOS.ID_PRODUCTO WHERE (CARRITO.NOMBRE_USUARIO = @NOMBRE_USUARIO)">
        <SelectParameters>
            <asp:SessionParameter SessionField="SCaja" Name="NOMBRE_USUARIO" Type="String"></asp:SessionParameter>
        </SelectParameters>
    </asp:SqlDataSource>

    <!-- Small modal -->
    <%--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>--%>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Eliminar producto</h4>
                </div>
                <div class="modal-body">
                    <h4>Está seguro que desea eliminar el producto?&hellip;</h4>
                    <p>
                        <asp:Label ID="lblNR" runat="server" Text="Label"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Si</button>
                    <button type="button" class="btn btn-primary">No</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

