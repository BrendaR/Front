﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ConfirmacionEliminar.aspx.vb" Inherits="Carrito_ConfimacionEliminar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br />
    <br />

    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <h4>Se eliminará del carrito el siguiente producto: </h4>
                <h3><asp:Label ID="lblNombre" runat="server" Text="Label" Font-Bold="true"></asp:Label></h3>
                <h4>Está seguro que desea eliminar el producto?</h4>
                <asp:Button ID="btnEliminar" runat="server" Text="Quitar del carrito" CssClass="btn btn-danger" />
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>