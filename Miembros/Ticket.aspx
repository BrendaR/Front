﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Ticket.aspx.vb" Inherits="Miembros_ConfirmarDatos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br /><hr />
    <div class="row">
        <div class="col-md-4">
            <asp:Image src="../img/logon.png" CssClass="img img-responsive" Width="200px" ID="Image1" runat="server" />
        </div>
        <div class="col-md-4">
            <h1 style="font-weight:500">Compra realizada!</h1>
        </div>
        <div class="col-md-4">
            <h3>Total de la compra: </h3>
            <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label><br />
        </div>
    </div><hr />
    <p style="font-size:larger; text-align:center">Tu compra a sido completada sigue las indicaciones enviadas a tu correo</p>
</asp:Content>

