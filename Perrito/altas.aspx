﻿<%@ Page Title="" Language="VB" MasterPageFile="SiteA.master" AutoEventWireup="false" CodeFile="altas.aspx.vb" Inherits="Perrito_altas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <h2>Altas de Productos</h2>
    <hr />
    <br />
    <br />

    <div class="form-horizontal">
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtNombreProd" CssClass="col-md-2 control-label">Nombre del Producto</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtNombreProd" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNombreProd"
                    CssClass="text-danger" ErrorMessage="El campo de nombre de producto es necesario." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ddlTipo" CssClass="col-md-2 control-label">Categoria</asp:Label>
            <div class="col-md-10">
                <asp:DropDownList ID="ddlTipo" runat="server" DataSourceID="SqlDataSource1" DataTextField="NOMBRE_CATEGORIA" DataValueField="ID_CATEGORIA"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT * FROM [CATEGORIA]"></asp:SqlDataSource>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtPrecio" CssClass="col-md-2 control-label">Precio</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtPrecio" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPrecio"
                    CssClass="text-danger" ErrorMessage="El campo precio es necesario." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="chkOferta" CssClass="col-md-2 control-label">Oferta</asp:Label>
            <div class="col-md-10">
                <asp:CheckBox ID="chkOferta" runat="server" CssClass="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtPrecioOf" CssClass="col-md-2 control-label">Precio Oferta</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtPrecioOf" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPrecioOf"
                    CssClass="text-danger" ErrorMessage="El campo precio oferta es necesario." />
            </div>
        </div>

         <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtDescripcion" CssClass="col-md-2 control-label">Descripcion</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtDescripcion" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDescripcion"
                    CssClass="text-danger" ErrorMessage="El campo descripcion es necesario." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtImagen" CssClass="col-md-2 control-label">Imagen</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="txtImagen" CssClass="form-control" Text="~/IMGPROD/NADA.JPG" />
            </div>
            <asp:FileUpload ID="FileUpload1" runat="server"/>
            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
            <asp:Button ID="btnSubirImagen" runat="server" Text="Subir Archivo" CssClass="btn btn-success" />

        </div>


        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" ID="btnInsertar" Text="Guardar Cambios" CssClass="btn btn-default" />
            </div>
        </div>
</asp:Content>

