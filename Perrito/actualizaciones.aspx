﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Perrito/SiteA.master" AutoEventWireup="false" CodeFile="actualizaciones.aspx.vb" Inherits="Perrito_actualizaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <br />
    <br />
    <h2>Actualizaciones</h2>
    <br />
    <p>Teclee el nombre del producto a buscar: </p>
    <asp:TextBox ID="txtBuscar" runat="server"></asp:TextBox>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" />
    <br />
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID_PRODUCTO" DataSourceID="SqlDataSourceActualizar" AllowPaging="True">
        <EditItemTemplate>
            ID_PRODUCTO:
            <asp:Label Text='<%# Eval("ID_PRODUCTO") %>' runat="server" ID="ID_PRODUCTOLabel1" /><br />
            NOMBRE:
            <asp:TextBox Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRETextBox" /><br />
            DESCRIPCION:
            <asp:TextBox Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONTextBox" /><br />
            ID_CATEGORIA:
            <asp:TextBox Text='<%# Bind("ID_CATEGORIA") %>' runat="server" ID="ID_CATEGORIATextBox" /><br />
            PRECIO_NORMAL:
            <asp:TextBox Text='<%# Bind("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALTextBox" /><br />
            OFERTA:
            <asp:CheckBox Checked='<%# Bind("OFERTA") %>' runat="server" ID="OFERTACheckBox" /><br />
            PRECIO_OFERTA:
            <asp:TextBox Text='<%# Bind("PRECIO_OFERTA") %>' runat="server" ID="PRECIO_OFERTATextBox" /><br />
            IMAGEN:
            <asp:TextBox Text='<%# Bind("IMAGEN") %>' runat="server" ID="IMAGENTextBox" /><br />
            <asp:LinkButton runat="server" Text="Actualizar" CommandName="Update" ID="UpdateButton" CausesValidation="True" />&nbsp;<asp:LinkButton runat="server" Text="Cancelar" CommandName="Cancel" ID="UpdateCancelButton" CausesValidation="False" />
        </EditItemTemplate>
        <InsertItemTemplate>
            NOMBRE:
            <asp:TextBox Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRETextBox" /><br />
            DESCRIPCION:
            <asp:TextBox Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONTextBox" /><br />
            ID_CATEGORIA:
            <asp:TextBox Text='<%# Bind("ID_CATEGORIA") %>' runat="server" ID="ID_CATEGORIATextBox" /><br />
            PRECIO_NORMAL:
            <asp:TextBox Text='<%# Bind("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALTextBox" /><br />
            OFERTA:
            <asp:CheckBox Checked='<%# Bind("OFERTA") %>' runat="server" ID="OFERTACheckBox" /><br />
            PRECIO_OFERTA:
            <asp:TextBox Text='<%# Bind("PRECIO_OFERTA") %>' runat="server" ID="PRECIO_OFERTATextBox" /><br />
            IMAGEN:
            <asp:TextBox Text='<%# Bind("IMAGEN") %>' runat="server" ID="IMAGENTextBox" /><br />
            <asp:LinkButton runat="server" Text="Insertar" CommandName="Insert" ID="InsertButton" CausesValidation="True" />&nbsp;<asp:LinkButton runat="server" Text="Cancelar" CommandName="Cancel" ID="InsertCancelButton" CausesValidation="False" />
        </InsertItemTemplate>
        <ItemTemplate>
            ID_PRODUCTO:
            <asp:Label Text='<%# Eval("ID_PRODUCTO") %>' runat="server" ID="ID_PRODUCTOLabel" /><br />
            NOMBRE:
            <asp:Label Text='<%# Bind("NOMBRE") %>' runat="server" ID="NOMBRELabel" /><br />
            DESCRIPCION:
            <asp:Label Text='<%# Bind("DESCRIPCION") %>' runat="server" ID="DESCRIPCIONLabel" /><br />
            ID_CATEGORIA:
            <asp:Label Text='<%# Bind("ID_CATEGORIA") %>' runat="server" ID="ID_CATEGORIALabel" /><br />
            PRECIO_NORMAL:
            <asp:Label Text='<%# Bind("PRECIO_NORMAL") %>' runat="server" ID="PRECIO_NORMALLabel" /><br />
            OFERTA:
            <asp:CheckBox Checked='<%# Bind("OFERTA") %>' runat="server" ID="OFERTACheckBox" Enabled="false" /><br />
            PRECIO_OFERTA:
            <asp:Label Text='<%# Bind("PRECIO_OFERTA") %>' runat="server" ID="PRECIO_OFERTALabel" /><br />
            IMAGEN:
            <asp:Label Text='<%# Bind("IMAGEN") %>' runat="server" ID="IMAGENLabel" /><br />
            <asp:LinkButton runat="server" Text="Editar" CommandName="Edit" ID="EditButton" CausesValidation="False" />&nbsp;<asp:LinkButton runat="server" Text="Eliminar" CommandName="Delete" ID="DeleteButton" CausesValidation="False" Visible="False" />&nbsp;<asp:LinkButton runat="server" Text="Nuevo" CommandName="New" ID="NewButton" CausesValidation="False" Visible="False" />
        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceActualizar" ConflictDetection="CompareAllValues" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' DeleteCommand="DELETE FROM [PRODUCTOS] WHERE [ID_PRODUCTO] = @original_ID_PRODUCTO AND [NOMBRE] = @original_NOMBRE AND [DESCRIPCION] = @original_DESCRIPCION AND [ID_CATEGORIA] = @original_ID_CATEGORIA AND [PRECIO_NORMAL] = @original_PRECIO_NORMAL AND [OFERTA] = @original_OFERTA AND [PRECIO_OFERTA] = @original_PRECIO_OFERTA AND [IMAGEN] = @original_IMAGEN" InsertCommand="INSERT INTO [PRODUCTOS] ([NOMBRE], [DESCRIPCION], [ID_CATEGORIA], [PRECIO_NORMAL], [OFERTA], [PRECIO_OFERTA], [IMAGEN]) VALUES (@NOMBRE, @DESCRIPCION, @ID_CATEGORIA, @PRECIO_NORMAL, @OFERTA, @PRECIO_OFERTA, @IMAGEN)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [PRODUCTOS]" UpdateCommand="UPDATE [PRODUCTOS] SET [NOMBRE] = @NOMBRE, [DESCRIPCION] = @DESCRIPCION, [ID_CATEGORIA] = @ID_CATEGORIA, [PRECIO_NORMAL] = @PRECIO_NORMAL, [OFERTA] = @OFERTA, [PRECIO_OFERTA] = @PRECIO_OFERTA, [IMAGEN] = @IMAGEN WHERE [ID_PRODUCTO] = @original_ID_PRODUCTO AND [NOMBRE] = @original_NOMBRE AND [DESCRIPCION] = @original_DESCRIPCION AND [ID_CATEGORIA] = @original_ID_CATEGORIA AND [PRECIO_NORMAL] = @original_PRECIO_NORMAL AND [OFERTA] = @original_OFERTA AND [PRECIO_OFERTA] = @original_PRECIO_OFERTA AND [IMAGEN] = @original_IMAGEN">
        <DeleteParameters>
            <asp:Parameter Name="original_ID_PRODUCTO" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="original_NOMBRE" Type="String"></asp:Parameter>
            <asp:Parameter Name="original_DESCRIPCION" Type="String"></asp:Parameter>
            <asp:Parameter Name="original_ID_CATEGORIA" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="original_PRECIO_NORMAL" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="original_OFERTA" Type="Boolean"></asp:Parameter>
            <asp:Parameter Name="original_PRECIO_OFERTA" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="original_IMAGEN" Type="String"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="NOMBRE" Type="String"></asp:Parameter>
            <asp:Parameter Name="DESCRIPCION" Type="String"></asp:Parameter>
            <asp:Parameter Name="ID_CATEGORIA" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="PRECIO_NORMAL" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="OFERTA" Type="Boolean"></asp:Parameter>
            <asp:Parameter Name="PRECIO_OFERTA" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="IMAGEN" Type="String"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="NOMBRE" Type="String"></asp:Parameter>
            <asp:Parameter Name="DESCRIPCION" Type="String"></asp:Parameter>
            <asp:Parameter Name="ID_CATEGORIA" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="PRECIO_NORMAL" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="OFERTA" Type="Boolean"></asp:Parameter>
            <asp:Parameter Name="PRECIO_OFERTA" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="IMAGEN" Type="String"></asp:Parameter>
            <asp:Parameter Name="original_ID_PRODUCTO" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="original_NOMBRE" Type="String"></asp:Parameter>
            <asp:Parameter Name="original_DESCRIPCION" Type="String"></asp:Parameter>
            <asp:Parameter Name="original_ID_CATEGORIA" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="original_PRECIO_NORMAL" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="original_OFERTA" Type="Boolean"></asp:Parameter>
            <asp:Parameter Name="original_PRECIO_OFERTA" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="original_IMAGEN" Type="String"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

