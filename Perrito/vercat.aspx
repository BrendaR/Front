﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Perrito/SiteA.master" AutoEventWireup="false" CodeFile="vercat.aspx.vb" Inherits="Perrito_vercat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br />
    <h2>Elija la categoria</h2><hr />
    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSourceCategorias" DataTextField="NOMBRE_CATEGORIA" DataValueField="ID_CATEGORIA" AutoPostBack="True"></asp:DropDownList><br />
    <br />
    <asp:SqlDataSource runat="server" ID="SqlDataSourceCategorias" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT * FROM [CATEGORIA] ORDER BY [NOMBRE_CATEGORIA]"></asp:SqlDataSource>
    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourceBuscarXCategoria">
        <AlternatingItemTemplate>
            <li style="background-color: #FFF8DC;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
            </li>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <li style="background-color: #008A8C; color: #FFFFFF;">NOMBRE:
                <asp:TextBox ID="NOMBRETextBox" runat="server" Text='<%# Bind("NOMBRE") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:TextBox ID="PRECIO_NORMALTextBox" runat="server" Text='<%# Bind("PRECIO_NORMAL") %>' />
                <br />
                DESCRIPCION:
                <asp:TextBox ID="DESCRIPCIONTextBox" runat="server" Text='<%# Bind("DESCRIPCION") %>' />
                <br />
                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Actualizar" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancelar" />
            </li>
        </EditItemTemplate>
        <EmptyDataTemplate>
            No se han devuelto datos.
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <li style="">NOMBRE:
                <asp:TextBox ID="NOMBRETextBox" runat="server" Text='<%# Bind("NOMBRE") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:TextBox ID="PRECIO_NORMALTextBox" runat="server" Text='<%# Bind("PRECIO_NORMAL") %>' />
                <br />
                DESCRIPCION:
                <asp:TextBox ID="DESCRIPCIONTextBox" runat="server" Text='<%# Bind("DESCRIPCION") %>' />
                <br />
                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insertar" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Borrar" />
            </li>
        </InsertItemTemplate>
        <ItemSeparatorTemplate>
            <br />
        </ItemSeparatorTemplate>
        <ItemTemplate>
            <li style="background-color: #DCDCDC; color: #000000;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
            </li>
        </ItemTemplate>
        <LayoutTemplate>
            <ul id="itemPlaceholderContainer" runat="server" style="font-family: Verdana, Arial, Helvetica, sans-serif;">
                <li runat="server" id="itemPlaceholder" />
            </ul>
            <div style="text-align: center; background-color: #CCCCCC; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000;">
                <asp:DataPager ID="DataPager1" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                    </Fields>
                </asp:DataPager>
            </div>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <li style="background-color: #008A8C; font-weight: bold; color: #FFFFFF;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
            </li>
        </SelectedItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceBuscarXCategoria" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [NOMBRE], [PRECIO_NORMAL], [DESCRIPCION] FROM [PRODUCTOS] WHERE ([ID_CATEGORIA] = @ID_CATEGORIA)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" PropertyName="SelectedValue" Name="ID_CATEGORIA" Type="Int32"></asp:ControlParameter>
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceDataList" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT * FROM [PRODUCTOS]"></asp:SqlDataSource>
</asp:Content>

