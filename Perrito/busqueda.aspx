﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Perrito/SiteA.master" AutoEventWireup="false" CodeFile="busqueda.aspx.vb" Inherits="Perrito_busqueda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br />
    <hr />
    <h2>Busqueda de productos</h2>
    <hr />
    <asp:Label ID="lblBuscar" runat="server" Text="Nombre del producto"></asp:Label>
    <asp:TextBox ID="txtBuscar" runat="server"></asp:TextBox>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" /><br /><br />
    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourceBuscar">
        <AlternatingItemTemplate>
            <li style="background-color: #FFF8DC;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                IMAGEN:
                <asp:Label ID="IMAGENLabel" runat="server" Text='<%# Eval("IMAGEN") %>' />
                <br />
            </li>
        </AlternatingItemTemplate>
        <EditItemTemplate>
            <li style="background-color: #008A8C;color: #FFFFFF;">NOMBRE:
                <asp:TextBox ID="NOMBRETextBox" runat="server" Text='<%# Bind("NOMBRE") %>' />
                <br />
                DESCRIPCION:
                <asp:TextBox ID="DESCRIPCIONTextBox" runat="server" Text='<%# Bind("DESCRIPCION") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:TextBox ID="PRECIO_NORMALTextBox" runat="server" Text='<%# Bind("PRECIO_NORMAL") %>' />
                <br />
                IMAGEN:
                <asp:TextBox ID="IMAGENTextBox" runat="server" Text='<%# Bind("IMAGEN") %>' />
                <br />
                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Actualizar" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancelar" />
            </li>
        </EditItemTemplate>
        <EmptyDataTemplate>
            No se han devuelto datos.
        </EmptyDataTemplate>
        <InsertItemTemplate>
            <li style="">NOMBRE:
                <asp:TextBox ID="NOMBRETextBox" runat="server" Text='<%# Bind("NOMBRE") %>' />
                <br />DESCRIPCION:
                <asp:TextBox ID="DESCRIPCIONTextBox" runat="server" Text='<%# Bind("DESCRIPCION") %>' />
                <br />PRECIO_NORMAL:
                <asp:TextBox ID="PRECIO_NORMALTextBox" runat="server" Text='<%# Bind("PRECIO_NORMAL") %>' />
                <br />IMAGEN:
                <asp:TextBox ID="IMAGENTextBox" runat="server" Text='<%# Bind("IMAGEN") %>' />
                <br />
                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insertar" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Borrar" />
            </li>
        </InsertItemTemplate>
        <ItemSeparatorTemplate>
<br />
        </ItemSeparatorTemplate>
        <ItemTemplate>
            <li style="background-color: #DCDCDC;color: #000000;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                IMAGEN:
                <asp:Label ID="IMAGENLabel" runat="server" Text='<%# Eval("IMAGEN") %>' />
                <br />
            </li>
        </ItemTemplate>
        <LayoutTemplate>
            <ul id="itemPlaceholderContainer" runat="server" style="font-family: Verdana, Arial, Helvetica, sans-serif;">
                <li runat="server" id="itemPlaceholder" />
            </ul>
            <div style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                <asp:DataPager ID="DataPager1" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" />
                        <asp:NumericPagerField />
                        <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="False" />
                    </Fields>
                </asp:DataPager>
            </div>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <li style="background-color: #008A8C;font-weight: bold;color: #FFFFFF;">NOMBRE:
                <asp:Label ID="NOMBRELabel" runat="server" Text='<%# Eval("NOMBRE") %>' />
                <br />
                DESCRIPCION:
                <asp:Label ID="DESCRIPCIONLabel" runat="server" Text='<%# Eval("DESCRIPCION") %>' />
                <br />
                PRECIO_NORMAL:
                <asp:Label ID="PRECIO_NORMALLabel" runat="server" Text='<%# Eval("PRECIO_NORMAL") %>' />
                <br />
                IMAGEN:
                <asp:Label ID="IMAGENLabel" runat="server" Text='<%# Eval("IMAGEN") %>' />
                <br />
            </li>
        </SelectedItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceBuscar" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT [NOMBRE], [DESCRIPCION], [PRECIO_NORMAL], [IMAGEN] FROM [PRODUCTOS] WHERE ([NOMBRE] LIKE '%' + @NOMBRE + '%')">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtBuscar" PropertyName="Text" Name="NOMBRE" Type="String"></asp:ControlParameter>
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

