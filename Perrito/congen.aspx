﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Perrito/SiteA.master" AutoEventWireup="false" CodeFile="congen.aspx.vb" Inherits="Perrito_congen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br />
    <br />
    <h2>Productos Registrados</h2>
    <hr />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID_PRODUCTO" DataSourceID="SqlDataSourceGridView">

        <Columns>
            <asp:BoundField DataField="ID_PRODUCTO" HeaderText="ID_PRODUCTO" ReadOnly="True" InsertVisible="False" SortExpression="ID_PRODUCTO"></asp:BoundField>
            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE"></asp:BoundField>
            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION"></asp:BoundField>
            <asp:BoundField DataField="ID_CATEGORIA" HeaderText="ID_CATEGORIA" SortExpression="ID_CATEGORIA"></asp:BoundField>
            <asp:BoundField DataField="PRECIO_NORMAL" HeaderText="PRECIO_NORMAL" SortExpression="PRECIO_NORMAL"></asp:BoundField>
            <asp:CheckBoxField DataField="OFERTA" HeaderText="OFERTA" SortExpression="OFERTA"></asp:CheckBoxField>
            <asp:BoundField DataField="PRECIO_OFERTA" HeaderText="PRECIO_OFERTA" SortExpression="PRECIO_OFERTA"></asp:BoundField>
            <asp:BoundField DataField="IMAGEN" HeaderText="IMAGEN" SortExpression="IMAGEN"></asp:BoundField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="SqlDataSourceGridView" ConnectionString='<%$ ConnectionStrings:MiBDConection %>' SelectCommand="SELECT * FROM [PRODUCTOS] ORDER BY [NOMBRE]"></asp:SqlDataSource>
</asp:Content>

