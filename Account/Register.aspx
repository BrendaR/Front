﻿<%@ Page Title="Register" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Register.aspx.vb" Inherits="Account_Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2>Registro</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Crea una nueva cuenta</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Nombre" CssClass="col-md-2 control-label">Nombre</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Nombre" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Nombre"
                    CssClass="text-danger" ErrorMessage="El nombre es requerido." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserName" CssClass="col-md-2 control-label">Nombre de Usuario</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName"
                    CssClass="text-danger" ErrorMessage="El nombre de usuario se requiere." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="lblmail" CssClass="col-md-2 control-label">E-mail</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="lblmail" TextMode="Email" CssClass="form-control" />
            </div>
        </div>
        
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="tel" CssClass="col-md-2 control-label">Teléfono</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="tel" TextMode="Phone" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tel"
                    CssClass="text-danger" ErrorMessage="El teléfono es requerido." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Contraseña</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="La contraseña se requiere." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirmar Contraseña</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Confirma tu contraseña." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Direccion" CssClass="col-md-2 control-label">Dirección</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Direccion" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Direccion"
                    CssClass="text-danger" ErrorMessage="La dirección es requerida." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Ciudad" CssClass="col-md-2 control-label">Ciudad</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Ciudad" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Ciudad"
                    CssClass="text-danger" ErrorMessage="La ciudad es requerida." />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="cp" CssClass="col-md-2 control-label">Código Postal</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="cp" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="cp"
                    CssClass="text-danger" ErrorMessage="El código postal es requerido." />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Registrarse" CssClass="btn btn-default" ID="btnUsuario" />
            </div>
        </div>
    </div>
</asp:Content>

