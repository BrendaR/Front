﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="MetodosEnvio.aspx.vb" Inherits="Carrito_MetodosEnvio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <br /><br /><br />
    <hr />
    <h3>Método de envio</h3>
    <hr />
    <div class="form-group">
        <asp:RadioButton ID="rb1" GroupName="Envio" runat="server" Text="Se recoge en sucursal $0" /><br />
        <asp:RadioButton ID="rb2" GroupName="Envio" runat="server" Text="Estafeta $150"  Checked="true"/><br />
        <asp:RadioButton ID="rb3" GroupName="Envio" runat="server" Text="FedEx $200" /><br />
        <asp:RadioButton ID="rb4" GroupName="Envio" runat="server" Text="DHL $180" /><br />
    </div>
    <br /><br /><hr />
    <h3>Forma de pago</h3>
    
    <div class="form-group">
<%--        <div class="col-md-10">--%>
            <asp:DropDownList ID="ddlPago" runat="server">
                <asp:ListItem>Tarjeta</asp:ListItem>
                <asp:ListItem>Oxxo</asp:ListItem>
                <asp:ListItem>Sucursal</asp:ListItem>
            </asp:DropDownList>
       <%-- </div>--%>
    </div>
    <br /><br />
    <div class="imgProd">
        <!-- Button trigger modal -->
        <asp:Button ID="btnContinuar" runat="server" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary" Text="Continuar" />
        <%--<a data-toggle="modal" data-target="#exampleModal" class="btn btn-primary">Continuar</a>--%>
        <asp:Button ID="btnCancelar" CssClass="btn btn-default" runat="server" Text="Cancelar" />
    </div>
    
    <%--modal--%>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                       <h3 class="modal-title" id="myModalTittle"> El total es de: </h3>
                           <asp:Label ID="lblTotalPagarT" runat="server" Text="Label"></asp:Label>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <%-- costo del envio --%>
                    Mensajeria seleccionada: 
                    <asp:Label ID="lblMensajeriaEnvio" runat="server" Text=""></asp:Label><br />
                    Método de pago: 
                    <asp:Label ID="lblMetodoPago" runat="server" Text=""></asp:Label>
                    <%--suma de lo que tiene en el carrito--%>
                    <hr />
                    <div class="row">
                        <div class="col-md-6 col-md-offset text-right">
                            <asp:Label ID="lblTotal" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                            <asp:Label ID="lblEnvio" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                            Pago TOTAL con envío: 
                            <asp:Label ID="lblTotalPagar" runat="server" Text="" Font-Bold="true"></asp:Label><br />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button ID="btnPagar" runat="server" Text="Pagar" CssClass="btn btn-primary"/>
                </div>
            </div>
        </div>
    </div>
</asp:Content>