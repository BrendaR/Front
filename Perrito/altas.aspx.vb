﻿
Partial Class Perrito_altas
    Inherits System.Web.UI.Page

    Private Sub btnInsertar_Click(sender As Object, e As EventArgs) Handles btnInsertar.Click
        Dim TA As New dsProductosTableAdapters.PRODUCTOSTableAdapter

        Dim Precio As Double
        Dim Precio_Oferta As Double

        Precio_Oferta = Val(txtPrecioOf.Text)
        Precio = Val(txtPrecio.Text)

        If Precio_Oferta > Precio Then
            Response.Write("<script>alert('el precio de oferta no puede ser mayor al precio regular');<script>")
            txtPrecio.Focus()
            Exit Sub
        End If

        TA.InsertarNuevoProducto(txtNombreProd.Text, txtDescripcion.Text, ddlTipo.SelectedValue, txtPrecio.Text, chkOferta.Checked, txtPrecioOf.Text, txtImagen.Text)

        Response.Write("<script>alert('El registro se insertó correctamente')<script>")
        'limpiar los campos
        txtNombreProd.Text = ""
        txtPrecio.Text = ""
        txtPrecioOf.Text = ""
        txtDescripcion.Text = ""
        chkOferta.Checked = False
    End Sub

    Private Sub btnSubirImagen_Click(sender As Object, e As EventArgs) Handles btnSubirImagen.Click
        'subir el archivo

        Dim path As String = Server.MapPath("~/img/")
        Dim fileOk As Boolean = False

        If FileUpload1.HasFile Then
            Dim fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() = {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOk = True
                End If
            Next
            If fileOk Then
                Try
                    FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)
                    lblMensaje.Text = "Archivo agregado!!"
                    txtImagen.Text = "~/img/" & FileUpload1.FileName
                    'imgFig.ImageUrl=txtImagen.Text
                Catch ex As Exception
                    lblMensaje.Text = "No se puede subir el archivo"
                End Try
            Else
                lblMensaje.Text = "No se acepta este tipo de archivo"
            End If
        End If
    End Sub
End Class
